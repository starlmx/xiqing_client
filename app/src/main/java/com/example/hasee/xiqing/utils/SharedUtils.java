package com.example.hasee.xiqing.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by wanxiang on 2017/4/15.
 */
//获取权限等全App信息
public class SharedUtils {
    public static final String FILE_NAME = "Xiqing";
    public static final String PRIVILEGE = "privilege";

    //读取权限信息
    public static int getPrivilege(Context context){
        return context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE).getInt(PRIVILEGE, 0);
    }
    //写入权限信息
    public static void putPrivilege(Context context, int privilege){
        SharedPreferences.Editor editor = context.getSharedPreferences(FILE_NAME, Context.MODE_APPEND).edit();
        editor.putInt(PRIVILEGE, privilege);
        editor.commit();
    }
}
