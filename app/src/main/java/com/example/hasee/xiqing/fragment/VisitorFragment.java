package com.example.hasee.xiqing.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hasee.xiqing.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.x;

/**
 * Created by wanxiang on 2017/4/15.
 */
    @ContentView(R.layout.fragment_visitor)
public class VisitorFragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = x.view().inject(this, inflater, container);
        return view;
    }
}
