package com.example.hasee.xiqing.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hasee.xiqing.R;
import com.example.hasee.xiqing.utils.SharedUtils;

import org.xutils.view.annotation.ContentView;
import org.xutils.x;


/**
 * Created by wanxiang on 2017/4/12.
 */
//食堂Fragment
    @ContentView(R.layout.fragment_canteen)
public class CanteenFragment extends Fragment {
    private Fragment mCurrent;
    private VisitorFragment mVisitor;
    private CanteenForChef mChef;
    private CanteenForCustomer mCustomer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = x.view().inject(this, inflater, container);
        init();
        return view;
    }

    //隐藏后重新显示时，刷新界面
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden){//显示时执行
            int privilege = SharedUtils.getPrivilege(x.app());
            Log.i("privilege...true ", privilege+"");
            //实现界面变更
            changeByPrivilege(privilege);
        }
    }

    private void init(){
        FragmentManager fm=getFragmentManager();
        FragmentTransaction transaction=fm.beginTransaction();
        mVisitor = new VisitorFragment();
        mCurrent = mVisitor;
        transaction.replace(R.id.canteen_content, mVisitor);
        transaction.commit();
    }

    //根据权限显示不同页面Fragment
    private void changeByPrivilege(int privilege){
        Fragment to=new Fragment();//目标Fragment
        FragmentManager fm=getFragmentManager();
        FragmentTransaction transaction=fm.beginTransaction();
        //判断选中的导航标签
        switch (privilege){
            case 0://游客
                if (mVisitor==null) mVisitor=new VisitorFragment();
                to = mVisitor;
                break;
            case 1://学生
                if (mCustomer==null) mCustomer=new CanteenForCustomer();
                to = mCustomer;
                break;
            case 2://厨师
                if (mChef==null) mChef=new CanteenForChef();
                to = mChef;
                break;
            case 3://维修工
                if (mCustomer==null) mCustomer=new CanteenForCustomer();
                to = mCustomer;
                break;
        }
        //根据选中的导航的不同用新的Fragment替换当前显示的页面
        if (mCurrent!=to){
            if (!to.isAdded()) {// 先判断是否被add过
                transaction.hide(mCurrent).add(R.id.canteen_content, to);// 隐藏当前的Fragment，add下一个到Activity中
            } else {
                transaction.hide(mCurrent).show(to);// 隐藏当前的fragment，显示目标Fragment
            }
        }
        transaction.commit();//Fragment切换事务提交
        mCurrent = to;
    }

}
