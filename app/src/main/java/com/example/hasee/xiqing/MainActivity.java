package com.example.hasee.xiqing;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.hasee.xiqing.fragment.CanteenFragment;
import com.example.hasee.xiqing.fragment.NewsFragment;
import com.example.hasee.xiqing.fragment.RepairFragment;
import com.example.hasee.xiqing.fragment.UserFragment;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;


//应用主页界面
public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    @ViewInject(R.id.bottom_group)
    RadioGroup bottomBar;
    @ViewInject(R.id.bottom_news)
    RadioButton button_news;

    private NewsFragment mNews;
    private UserFragment mUser;
    private RepairFragment mRepair;
    private CanteenFragment mCanteen;
    private Fragment mCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        x.view().inject(this);
        setDefaultFragment();//设置初始Fragment为新闻页面
        bottomBar.setOnCheckedChangeListener(this);


    }
    //默认显示动态页
    private void setDefaultFragment(){
        button_news.setChecked(true);//设置新闻导航标签为默认选中
        //用新闻Fragment替换当前显示的页面
        FragmentManager fm=getFragmentManager();
        FragmentTransaction transaction=fm.beginTransaction();
        mNews=new NewsFragment();
        mCurrent=mNews;
        transaction.replace(R.id.frame_content, mNews);
        transaction.commit();//Fragment切换事务提交
    }
    //根据底部导航选项切换显示页面
    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        Fragment to=new Fragment();//目标Fragment
        FragmentManager fm=getFragmentManager();
        FragmentTransaction transaction=fm.beginTransaction();
        //判断选中的导航标签
        switch (checkedId){
            case R.id.bottom_news:
                if (mNews==null) mNews=new NewsFragment();
                to = mNews;
                break;
            case R.id.bottom_canteen:
                if (mCanteen==null) mCanteen=new CanteenFragment();
                to = mCanteen;
                break;
            case R.id.bottom_repair:
                if (mRepair==null) mRepair=new RepairFragment();
                to = mRepair;
                break;
            case R.id.bottom_user:
                if (mUser==null) mUser=new UserFragment();
                to = mUser;
                break;
        }
        //根据选中的导航的不同用新的Fragment替换当前显示的页面
        if (!to.isAdded()) {// 先判断是否被add过
            transaction.hide(mCurrent).add(R.id.frame_content, to);// 隐藏当前的Fragment，add下一个到Activity中
        } else {
            transaction.hide(mCurrent).show(to);// 隐藏当前的fragment，显示目标Fragment
        }
        transaction.commit();//Fragment切换事务提交
        mCurrent = to;
    }
}
