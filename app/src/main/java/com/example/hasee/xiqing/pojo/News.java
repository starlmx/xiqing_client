package com.example.hasee.xiqing.pojo;


/**
 * Created by wanxiang on 2017/4/14.
 */

public class News {
    private String title;
    private String date;
    private String link;
    private String catgory;

    public News() {
    }

    public News(String title, String date, String link, String catgory) {
        this.title = title;
        this.date = date;
        this.link = link;
        this.catgory = catgory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCatgory() {
        return catgory;
    }

    public void setCatgory(String catgory) {
        this.catgory = catgory;
    }

}
