package com.example.hasee.xiqing.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hasee.xiqing.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.x;


/**
 * Created by wanxiang on 2017/4/12.
 */
//报修Fragment
    @ContentView(R.layout.fragment_repair)
public class RepairFragment extends Fragment {
    private Fragment mCurrent;
    private VisitorFragment mVisitor;
    private RepairForCustomer mCustomer;
    private RepairForRepairman mRepairman;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = x.view().inject(this, inflater, container);
        return view;
    }

    private void init(){
        FragmentManager fm=getFragmentManager();
        FragmentTransaction transaction=fm.beginTransaction();
        mVisitor = new VisitorFragment();
        mCurrent = mVisitor;
        transaction.replace(R.id.repair_content, mVisitor);
        transaction.commit();
    }
}
