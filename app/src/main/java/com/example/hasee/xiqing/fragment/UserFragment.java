package com.example.hasee.xiqing.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import com.example.hasee.xiqing.R;
import com.example.hasee.xiqing.TestActivity;
import com.example.hasee.xiqing.utils.SharedUtils;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * Created by wanxiang on 2017/4/12.
 */
//用户Fragment
public class UserFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        x.view().inject(this, view);
        return view;
    }
}
