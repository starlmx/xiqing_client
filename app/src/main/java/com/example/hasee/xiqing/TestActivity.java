package com.example.hasee.xiqing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

public class TestActivity extends AppCompatActivity {
    @ViewInject(R.id.test_tv)
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        x.view().inject(this);
        Intent intent = getIntent();
        String msg=intent.getStringExtra("msg");
        //TextView tv=(TextView) findViewById(R.id.test_tv);
        tv.setText(msg);
    }
}
