package com.example.hasee.xiqing;

import android.app.Application;

import com.example.hasee.xiqing.utils.SharedUtils;

import org.xutils.x;

/**
 * Created by wanxiang on 2017/4/13.
 */
public class MyApplication extends Application {
    private static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.init(this);//初始化XUtils
        x.Ext.setDebug(false);//关闭Debug输出
        instance = this;
        SharedUtils.putPrivilege(this, 0);
    }
    //在整个项目中获得Application上下文
    public static MyApplication getInstance(){
        // 因为我们程序运行后，Application是首先初始化的，如果在这里不用判断instance是否为空
        return instance;
    }
}
